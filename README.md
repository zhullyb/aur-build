# Aur-build

Use gitlab ci to build aur packages. This is just a template to Test.

Makepkg can not run via root, you should use `sed -i '/E_ROOT/d' /usr/bin/makepkg` to hack it.

Makepkg can not resolve dependencies on AUR, please fix it manually.
